﻿Imports System.ComponentModel

Public Class Form2
    Dim pregunta(9) As String
    Dim respcorrecta(9) As Integer
    Dim respincorrecta(9) As Integer
    Dim totrespcontes(9) As Integer
    Dim totrespnocontes(9) As Integer
    Public Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click


        'Entrada de Datos
        pregunta(0) = txtpreguta1.Text
        pregunta(1) = txtpregunta2.Text
        pregunta(2) = txtpregunta3.Text
        pregunta(3) = txtpregunta4.Text
        pregunta(4) = txtpregunta5.Text
        pregunta(5) = txtpregunta6.Text
        pregunta(6) = txtpregunta7.Text
        pregunta(7) = txtpregunta8.Text
        pregunta(8) = txtpregunta9.Text
        pregunta(9) = txtpregunta10.Text

        'Proceso de datos



        If (pregunta(0) = "Función IF") Then
            txtpreguta1.BackColor = Color.Green
            respcorrecta(0) = 1
        Else
            totrespcontes(0) = 1
            txtpreguta1.BackColor = Color.Red
            respincorrecta(0) = 1
        End If

        If (pregunta(1) = "Concatenación") Then

            txtpregunta2.BackColor = Color.Green
            respcorrecta(1) = 1
        Else

            txtpregunta2.BackColor = Color.Red
            respincorrecta(1) = 1
        End If
        If (pregunta(2) = "Sobre dos operandos") Then

            txtpregunta3.BackColor = Color.Green
            respcorrecta(2) = 1

        Else
            txtpregunta3.BackColor = Color.Red
            respincorrecta(2) = 1
        End If
        If (pregunta(3) = "Intellisense") Then

            txtpregunta4.BackColor = Color.Green
            respcorrecta(3) = 1

        Else
            txtpregunta4.BackColor = Color.Red
            respincorrecta(3) = 1
        End If
        If (pregunta(4) = "Igual") Then

            txtpregunta5.BackColor = Color.Green
            respcorrecta(4) = 1

        Else
            txtpregunta5.BackColor = Color.Red
            respincorrecta(4) = 1
        End If
        If (pregunta(5) = "Paréntesis") Then

            txtpregunta6.BackColor = Color.Green
            respcorrecta(5) = 1

        Else
            txtpregunta6.BackColor = Color.Red
            respincorrecta(5) = 1
        End If
        If (pregunta(6) = "Dot Notation") Then

            txtpregunta7.BackColor = Color.Green
            respcorrecta(6) = 1

        Else
            txtpregunta7.BackColor = Color.Red
            respincorrecta(6) = 1
        End If
        If (pregunta(7) = "Suma") Then

            txtpregunta8.BackColor = Color.Green
            respcorrecta(7) = 1
        Else
            txtpregunta8.BackColor = Color.Red
            respincorrecta(7) = 1
        End If
        If (pregunta(8) = "Comillas") Then

            txtpregunta9.BackColor = Color.Green
            respcorrecta(8) = 1

        Else
            txtpregunta9.BackColor = Color.Red
            respincorrecta(8) = 1
        End If
        If (pregunta(9) = "AND y OR") Then

            txtpregunta10.BackColor = Color.Green
            respcorrecta(9) = 1
        Else
            txtpregunta10.BackColor = Color.Red
            respincorrecta(9) = 1
        End If




        Module1.totrepcorrec = (respcorrecta(0) + respcorrecta(1) + respcorrecta(2) + respcorrecta(3) + respcorrecta(4) + respcorrecta(5) + respcorrecta(6) + respcorrecta(7) + respcorrecta(8) + respcorrecta(9))
        Module1.porpregcorrectas = (respcorrecta(0) + respcorrecta(1) + respcorrecta(2) + respcorrecta(3) + respcorrecta(4) + respcorrecta(5) + respcorrecta(6) + respcorrecta(7) + respcorrecta(8) + respcorrecta(9)) * 100 / 10
        Module1.totrepincorrec = (respincorrecta(0) + respincorrecta(1) + respincorrecta(2) + respincorrecta(3) + respincorrecta(4) + respincorrecta(5) + respincorrecta(6) + respincorrecta(7) + respincorrecta(8) + respincorrecta(9))
        Module1.totpregcontes = (totrespcontes(0) + totrespcontes(1) + totrespcontes(2) + totrespcontes(3) + totrespcontes(4) + totrespcontes(5) + totrespcontes(6) + totrespcontes(7) + totrespcontes(8) + totrespcontes(9))
    End Sub

    Public Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim a As New Form3

        a.Show()
        Me.Finalize()


    End Sub

    Private Sub txtpreguta1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtpreguta1.SelectedIndexChanged

    End Sub

    Public Sub txtpreguta1_Validating(sender As Object, e As CancelEventArgs) Handles txtpreguta1.Validating
        If DirectCast(sender, ComboBox).Text.Length > 0 Then
            totrespcontes(0) = 1
        Else
            totrespnocontes(0) = 1
        End If
    End Sub

    Private Sub txtpregunta2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtpregunta2.SelectedIndexChanged

    End Sub

    Private Sub txtpregunta2_Validating(sender As Object, e As CancelEventArgs) Handles txtpregunta2.Validating
        If DirectCast(sender, ComboBox).Text.Length > 0 Then
            totrespcontes(1) = 1
        Else
            totrespnocontes(1) = 1
        End If
    End Sub

    Private Sub txtpregunta3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtpregunta3.SelectedIndexChanged

    End Sub

    Private Sub txtpregunta3_Validating(sender As Object, e As CancelEventArgs) Handles txtpregunta3.Validating
        If DirectCast(sender, ComboBox).Text.Length > 0 Then
            totrespcontes(2) = 1
        Else
            totrespnocontes(2) = 1
        End If
    End Sub

    Private Sub txtpregunta4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtpregunta4.SelectedIndexChanged

    End Sub

    Private Sub txtpregunta4_Validating(sender As Object, e As CancelEventArgs) Handles txtpregunta4.Validating
        If DirectCast(sender, ComboBox).Text.Length > 0 Then
            totrespcontes(3) = 1
        Else
            totrespnocontes(3) = 1
        End If
    End Sub

    Private Sub txtpregunta5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtpregunta5.SelectedIndexChanged

    End Sub

    Private Sub txtpregunta5_Validating(sender As Object, e As CancelEventArgs) Handles txtpregunta5.Validating
        If DirectCast(sender, ComboBox).Text.Length > 0 Then
            totrespcontes(4) = 1
        Else
            totrespnocontes(4) = 1
        End If
    End Sub

    Private Sub txtpregunta6_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtpregunta6.SelectedIndexChanged

    End Sub

    Private Sub txtpregunta6_Validating(sender As Object, e As CancelEventArgs) Handles txtpregunta6.Validating
        If DirectCast(sender, ComboBox).Text.Length > 0 Then
            totrespcontes(5) = 1
        Else
            totrespnocontes(5) = 1
        End If
    End Sub

    Private Sub txtpregunta7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtpregunta7.SelectedIndexChanged

    End Sub

    Private Sub txtpregunta7_Validating(sender As Object, e As CancelEventArgs) Handles txtpregunta7.Validating
        If DirectCast(sender, ComboBox).Text.Length > 0 Then
            totrespcontes(6) = 1
        Else
            totrespnocontes(6) = 1
        End If
    End Sub

    Private Sub txtpregunta8_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtpregunta8.SelectedIndexChanged

    End Sub

    Private Sub txtpregunta8_Validating(sender As Object, e As CancelEventArgs) Handles txtpregunta8.Validating
        If DirectCast(sender, ComboBox).Text.Length > 0 Then
            totrespcontes(7) = 1
        Else
            totrespnocontes(7) = 1
        End If
    End Sub

    Private Sub txtpregunta9_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtpregunta9.SelectedIndexChanged

    End Sub

    Private Sub txtpregunta9_Validating(sender As Object, e As CancelEventArgs) Handles txtpregunta9.Validating
        If DirectCast(sender, ComboBox).Text.Length > 0 Then
            totrespcontes(8) = 1
        Else
            totrespnocontes(8) = 1
        End If
    End Sub

    Private Sub txtpregunta10_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtpregunta10.SelectedIndexChanged

    End Sub

    Private Sub txtpregunta10_Validating(sender As Object, e As CancelEventArgs) Handles txtpregunta10.Validating
        If DirectCast(sender, ComboBox).Text.Length > 0 Then
            totrespcontes(9) = 1
        Else
            totrespnocontes(9) = 1
        End If
    End Sub
End Class