﻿Public Class Form1
    Dim Contador As Integer
    Private pass As String = "1234"

    Public Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        If (txtcontraseña.Text = pass) Then
            Dim a As New Form2

            a.Show()
            Me.Finalize()
        ElseIf (Contador > 1)
            txtintentos.Text = "Su contraseña fue invertida "
            MsgBox("Su contraseña fue invertida", MessageBoxIcon.Information)
            pass = StrReverse(pass)

        Else
            MsgBox("La contraseña es incorrecta", MessageBoxIcon.Information)

            Contador += 1
            txtintentos.Text = "Usted lleva " & Contador & " intentos de 3"
        End If
    End Sub

End Class
