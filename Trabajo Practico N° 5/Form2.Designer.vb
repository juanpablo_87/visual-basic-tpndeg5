﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtpreguta1 = New System.Windows.Forms.ComboBox()
        Me.txtpregunta2 = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtpregunta3 = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtpregunta4 = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtpregunta5 = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtpregunta6 = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtpregunta7 = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtpregunta8 = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtpregunta9 = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtpregunta10 = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(133, 621)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Ingresar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(421, 621)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 21
        Me.Button2.Text = "Porcentajes"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(165, 22)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(311, 24)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = "Cuestionario sobre Visual  Basic"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(72, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(506, 13)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "¿Qué estructura de control permite decidir entre dos opciones resultantes de una " &
    "sentencia de condición?"
        '
        'txtpreguta1
        '
        Me.txtpreguta1.BackColor = System.Drawing.Color.White
        Me.txtpreguta1.FormattingEnabled = True
        Me.txtpreguta1.Items.AddRange(New Object() {"Operador And", "Función IF", "Operador Not"})
        Me.txtpreguta1.Location = New System.Drawing.Point(75, 81)
        Me.txtpreguta1.Name = "txtpreguta1"
        Me.txtpreguta1.Size = New System.Drawing.Size(503, 21)
        Me.txtpreguta1.TabIndex = 24
        '
        'txtpregunta2
        '
        Me.txtpregunta2.FormattingEnabled = True
        Me.txtpregunta2.Items.AddRange(New Object() {"Unión", "Concatenación", "Texto"})
        Me.txtpregunta2.Location = New System.Drawing.Point(75, 134)
        Me.txtpregunta2.Name = "txtpregunta2"
        Me.txtpregunta2.Size = New System.Drawing.Size(503, 21)
        Me.txtpregunta2.TabIndex = 26
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(72, 117)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(300, 13)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "Las cadenas de texto se pueden unir utilizando el operador de"
        '
        'txtpregunta3
        '
        Me.txtpregunta3.FormattingEnabled = True
        Me.txtpregunta3.Items.AddRange(New Object() {"Sobre un operando", "Sobre dos operandos", "Sobre tres operandos"})
        Me.txtpregunta3.Location = New System.Drawing.Point(75, 193)
        Me.txtpregunta3.Name = "txtpregunta3"
        Me.txtpregunta3.Size = New System.Drawing.Size(503, 21)
        Me.txtpregunta3.TabIndex = 28
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(72, 176)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(302, 13)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "¿Sobre cuantos operandos actuan los operadores aritméticos?"
        '
        'txtpregunta4
        '
        Me.txtpregunta4.FormattingEnabled = True
        Me.txtpregunta4.Items.AddRange(New Object() {"Dot Notation", "Intellisense", "Función If"})
        Me.txtpregunta4.Location = New System.Drawing.Point(75, 246)
        Me.txtpregunta4.Name = "txtpregunta4"
        Me.txtpregunta4.Size = New System.Drawing.Size(503, 21)
        Me.txtpregunta4.TabIndex = 30
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(72, 229)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(283, 13)
        Me.Label4.TabIndex = 29
        Me.Label4.Text = "Ayuda a escribir el código más rápido y con menos errores."
        '
        'txtpregunta5
        '
        Me.txtpregunta5.FormattingEnabled = True
        Me.txtpregunta5.Items.AddRange(New Object() {"Suma", "Igual", "Concatenación"})
        Me.txtpregunta5.Location = New System.Drawing.Point(75, 296)
        Me.txtpregunta5.Name = "txtpregunta5"
        Me.txtpregunta5.Size = New System.Drawing.Size(503, 21)
        Me.txtpregunta5.TabIndex = 32
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(72, 279)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(377, 13)
        Me.Label5.TabIndex = 31
        Me.Label5.Text = "Los operadores y los operandos sólo se pueden colocar a la derecha del signo"
        '
        'txtpregunta6
        '
        Me.txtpregunta6.FormattingEnabled = True
        Me.txtpregunta6.Items.AddRange(New Object() {"Suma", "Paréntesis", "Multiplicación"})
        Me.txtpregunta6.Location = New System.Drawing.Point(75, 356)
        Me.txtpregunta6.Name = "txtpregunta6"
        Me.txtpregunta6.Size = New System.Drawing.Size(503, 21)
        Me.txtpregunta6.TabIndex = 34
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(72, 339)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(173, 13)
        Me.Label6.TabIndex = 33
        Me.Label6.Text = "¿Qué operación se realiza primero?"
        '
        'txtpregunta7
        '
        Me.txtpregunta7.FormattingEnabled = True
        Me.txtpregunta7.Items.AddRange(New Object() {"Dot Notation", "Intellisense", "Función If"})
        Me.txtpregunta7.Location = New System.Drawing.Point(75, 409)
        Me.txtpregunta7.Name = "txtpregunta7"
        Me.txtpregunta7.Size = New System.Drawing.Size(503, 21)
        Me.txtpregunta7.TabIndex = 36
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(72, 392)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(275, 13)
        Me.Label7.TabIndex = 35
        Me.Label7.Text = "Nos ayuda a conectar los controles con sus propiedades"
        '
        'txtpregunta8
        '
        Me.txtpregunta8.FormattingEnabled = True
        Me.txtpregunta8.Items.AddRange(New Object() {"And", "Suma", "NOT"})
        Me.txtpregunta8.Location = New System.Drawing.Point(75, 468)
        Me.txtpregunta8.Name = "txtpregunta8"
        Me.txtpregunta8.Size = New System.Drawing.Size(503, 21)
        Me.txtpregunta8.TabIndex = 38
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(72, 451)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(128, 13)
        Me.Label8.TabIndex = 37
        Me.Label8.Text = "Un operador aritmético es"
        '
        'txtpregunta9
        '
        Me.txtpregunta9.FormattingEnabled = True
        Me.txtpregunta9.Items.AddRange(New Object() {"Paréntesis", "Comillas", "Corchetes"})
        Me.txtpregunta9.Location = New System.Drawing.Point(75, 521)
        Me.txtpregunta9.Name = "txtpregunta9"
        Me.txtpregunta9.Size = New System.Drawing.Size(503, 21)
        Me.txtpregunta9.TabIndex = 40
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(72, 504)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(218, 13)
        Me.Label9.TabIndex = 39
        Me.Label9.Text = "Las cadenas de texto deben colocarse entre"
        '
        'txtpregunta10
        '
        Me.txtpregunta10.FormattingEnabled = True
        Me.txtpregunta10.Items.AddRange(New Object() {"AND y NOT", "OR y NOT", "AND y OR"})
        Me.txtpregunta10.Location = New System.Drawing.Point(75, 571)
        Me.txtpregunta10.Name = "txtpregunta10"
        Me.txtpregunta10.Size = New System.Drawing.Size(503, 21)
        Me.txtpregunta10.TabIndex = 42
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(72, 554)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(328, 13)
        Me.Label10.TabIndex = 41
        Me.Label10.Text = "Cuáles son los operadores booleanos que actúan en dos operandos"
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(643, 656)
        Me.Controls.Add(Me.txtpregunta10)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtpregunta9)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtpregunta8)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtpregunta7)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtpregunta6)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtpregunta5)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtpregunta4)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtpregunta3)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtpregunta2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtpreguta1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form2"
        Me.Text = "Cuestionario"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtpreguta1 As ComboBox
    Friend WithEvents txtpregunta2 As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtpregunta3 As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtpregunta4 As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtpregunta5 As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtpregunta6 As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtpregunta7 As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtpregunta8 As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtpregunta9 As ComboBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtpregunta10 As ComboBox
    Friend WithEvents Label10 As Label
End Class
